# Pragma Data Engineering Test

This project is an API built with FastAPI that allows loading data from CSV files to a PostgreSQL database, and then retrieving that data.
I decided to use FastAPI because I'm learning it and I think it's a great tool to build APIs.

## Requirements

- Python 3.10
- PostgreSQL
- Docker
- Docker Compose
- Libraries listed in `requirements.txt`

## Database Schema

The database consists of four tables: `prices`, `statistics`, `prices_validation`, and `statistics_validation`.

### `prices` Table

- `id`: UUID, primary key.
- `created_at`: TIMESTAMP.
- `user_id`: INT.
- `price`: FLOAT.
- `source_file`: TEXT.

### `statistics` Table

- `row_count`: INT.
- `avg_price`: FLOAT.
- `min_price`: FLOAT.
- `max_price`: FLOAT.

### `prices_validation` Table

- `id`: UUID, primary key.
- `created_at`: TIMESTAMP.
- `user_id`: INT.
- `price`: FLOAT.
- `source_file`: TEXT.

### `statistics_validation` Table

- `row_count`: INT.
- `avg_price`: FLOAT.
- `min_price`: FLOAT.
- `max_price`: FLOAT.

## Project Structure

The project has the following file and directory structure:

- `data/`: Directory containing the CSV files to be loaded into the database.
- `init.sql`: SQL file that creates the tables in the database.
- `pragma_main.py`: Main Python file containing the API logic.
- `requirements.txt`: File listing the Python dependencies required to run the project.
- `Dockerfile`: File used to build the Docker image.
- `docker-compose.yml`: File used to run the Docker container.
- `README.md`: This file.

## Docker

This project includes a `Dockerfile` and `docker-compose.yml` file to create and run a PostgreSQL DB in a Docker container.

To build and run the container, execute the following command in the project's root directory:

```bash
docker-compose up --build
```

Then you can access the API docs using the following URL:

```bash
http://localhost:5001/docs
```

If you have any trouble accessing the API docs, you should verify that the mo_data_eng_challenge-app image is running, you can check it with the following command:

```bash
docker ps
```

## Usage
1. Execute docker-compose up --build.  
2. Access the API documentation at http://localhost:5001/docs. You will see a list of available endpoints and their parameters.  
   - **GET** `/insert_data_into_db`: Inserts the CSV data (except validation.csv) into the PostgresDB.
     - **Parameter**: `chunk_size` to determine how many rows will be processed at a time.
   - **GET** `/get_data`: Returns data from the prices table or statistics table.
     - `Parameter`: `table` to decide if you want to retrieve data from `prices` or `statistics` tables.
   - **GET** `/insert_data_into_db_validation`: Works like `/insert_data_into_db`, but inserts `validation.csv` into the database.
   - **GET** `/get_validation_data`: Works like `/get_data`, but retrieves data from validation.csv.
   - **GET** `/get_overall_statistics`: Returns general statistics from the `prices` or `prices_validation` tables.
     - `Parameter`: `table_statistics` to get statistics from `csv` or `validation.csv`.
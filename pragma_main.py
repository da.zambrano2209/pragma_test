from fastapi import FastAPI, Query, HTTPException
import uuid
import pandas as pd
import psycopg2
from psycopg2 import sql
from psycopg2.extras import DictCursor
import logging
import os

logging.basicConfig(level=logging.INFO)
app = FastAPI(title="Pragma Prueba de ingeniería de datos")


def get_db_connection():
    db_name = os.getenv("DB_NAME")
    db_user = os.getenv("DB_USER")
    db_password = os.getenv("DB_PASSWORD")
    db_host = os.getenv("DB_HOST")
    db_port = os.getenv("DB_PORT")
    conn = psycopg2.connect(f"dbname={db_name} user={db_user} password={db_password} host={db_host} port={db_port}")
    return conn


@app.get("/insert_data_into_db", description="Get data from a csv file, the data is stored into Postgres DB.")
async def get_data(chunk_size: int = Query(None, description="Chunk size to be processed", ge=1, le=10)):
    try:
        if chunk_size < 1 or chunk_size > 10:
            raise HTTPException(status_code=400, detail="chunk_size must be between 1 and 10")
        row_count, total_price, min_price, max_price = 0, 0, None, None
        conn = get_db_connection()
        cur = conn.cursor()
        logging.info("get_data called with chunk_size=%s", chunk_size)

        for filename in os.listdir('data'):
            logging.error("File name to be processed: %s", filename)
            if not filename.startswith('validation'):
                for chunk in pd.read_csv(f'data/{filename}', chunksize=chunk_size):
                    chunk = chunk.dropna(
                        subset=['price'])  # Drop rows with NaN values in price column to avoid errors on statistics
                    chunk['source_file'] = filename
                    chunk['id'] = [str(uuid.uuid4()) for _ in range(len(chunk))]

                    data_tuples = [(row['id'], row['timestamp'], row['user_id'], row['price'], row['source_file']) for
                                   index, row in chunk.iterrows()]
                    row_count += len(chunk)
                    total_price += chunk['price'].sum()
                    min_price = min(min_price, chunk['price'].min()) if min_price is not None else chunk['price'].min()
                    max_price = max(max_price, chunk['price'].max()) if max_price is not None else chunk['price'].max()
                    statistics_data_tuples = [(int(row_count)), float(total_price), float(min_price),
                                              float(max_price)]

                    insert_query_prices = sql.SQL(
                        "INSERT INTO public.prices (id, created_at, user_id, price, source_file) \
                        VALUES (%s, %s, %s, %s, %s)")
                    cur.executemany(insert_query_prices, data_tuples)
                    conn.commit()
                    logging.info("Inserted row with id=%s", len(data_tuples))

                    insert_query_statistics = sql.SQL(
                        "INSERT INTO public.statistics (row_count, avg_price, min_price, max_price) \
                        VALUES (%s, %s, %s, %s)")
                    cur.execute(insert_query_statistics, statistics_data_tuples)
                    conn.commit()
                    logging.info("Inserted row_count, avg_price, min_price, max_price %s", statistics_data_tuples)

        conn.close()
        return "Data inserted into DB successfully!"
    except Exception as e:
        logging.error("Error: %s", e)
        return "Data not inserted into DB!"


@app.get("/get_data", description="Get data from prices table or statistics table from the DB.")
async def get_data(table: str = Query(None, description="Table name to get data from", regex="prices|statistics")):
    try:
        conn = get_db_connection()
        cur = conn.cursor(cursor_factory=DictCursor)

        if table == "prices":
            cur.execute("SELECT * FROM public.prices")
        elif table == "statistics":
            cur.execute("SELECT * FROM public.statistics")
        else:
            return {"error": "Invalid data_type. Choose between 'statistics' or 'prices'."}

        data = cur.fetchall()
        conn.close()
        return [dict(row) for row in data]
    except Exception as e:
        logging.error("Error: %s", e)
        return "Error getting tables from DB!"


@app.get("/insert_data_into_db_validation",
         description="Get data from validation csv file, the data is stored into Postgres DB.")
async def get_data(chunk_size: int = Query(None, description="Chunk size to be processed", ge=1, le=1)):
    try:
        if chunk_size != 1:
            raise HTTPException(status_code=400, detail="chunk_size must be between 1 and 10")
        row_count, total_price, min_price, max_price = 0, 0, None, None
        conn = get_db_connection()
        cur = conn.cursor()
        logging.info("get_data called with chunk_size=%s", chunk_size)

        for filename in os.listdir('data'):
            logging.error("File name to be processed: %s", filename)
            if filename.startswith('validation'):
                for chunk in pd.read_csv(f'data/{filename}', chunksize=chunk_size):
                    chunk['source_file'] = filename
                    chunk['id'] = [str(uuid.uuid4()) for _ in range(len(chunk))]
                    data_tuples = [(row['id'], row['timestamp'], row['user_id'], row['price'], row['source_file']) for
                                   index, row in chunk.iterrows()]
                    row_count += len(chunk)
                    total_price += chunk['price'].sum()
                    min_price = min(min_price, chunk['price'].min()) if min_price is not None else chunk['price'].min()
                    max_price = max(max_price, chunk['price'].max()) if max_price is not None else chunk['price'].max()
                    statistics_data_tuples = [(int(row_count)), float(total_price), float(min_price),
                                              float(max_price)]

                    insert_query_prices = sql.SQL(
                        "INSERT INTO public.prices_validation (id, created_at, user_id, price, source_file) \
                        VALUES (%s, %s, %s, %s, %s)")
                    cur.executemany(insert_query_prices, data_tuples)
                    conn.commit()
                    logging.info("Inserted row with id=%s", len(data_tuples))

                    insert_query_statistics = sql.SQL(
                        "INSERT INTO public.statistics_validation (row_count, avg_price, min_price, max_price) \
                        VALUES (%s, %s, %s, %s)")
                    cur.execute(insert_query_statistics, statistics_data_tuples)
                    conn.commit()
                    logging.info("Inserted row_count, avg_price, min_price, max_price %s", statistics_data_tuples)
        conn.close()
        return "Validation Data inserted into DB successfully!"
    except Exception as e:
        logging.error("Error: %s", e)
        return "Validation Data not inserted into DB!"


@app.get("/get_validation_data",
         description="Get data from prices_validation table or statistics_validation table from the DB.")
async def get_data(table: str = Query(None, description="Table name to get data from", regex="prices|statistics")):
    try:
        conn = get_db_connection()
        cur = conn.cursor(cursor_factory=DictCursor)

        if table == "prices":
            cur.execute("SELECT * FROM public.prices_validation")
        elif table == "statistics":
            cur.execute("SELECT * FROM public.statistics_validation")
        else:
            return {"error": "Invalid data_type. Choose between 'statistics' or 'prices'."}

        data = cur.fetchall()
        conn.close()
        return [dict(row) for row in data]
    except Exception as e:
        logging.error("Error: %s", e)
        return "Error getting tables from DB!"


@app.get("/get_overall_statictics",
         description="Get overall statistics from the prices table or prices_validation table.")
async def get_overall_statictics(
        table_statistics: str = Query(None, description="Table name to get data from", regex="csv|validation")):
    try:
        conn = get_db_connection()
        cur = conn.cursor()

        if table_statistics == "csv":
            cur.execute("SELECT COUNT(id), AVG(price), MIN(price), MAX(price) FROM public.prices")
        elif table_statistics == "validation":
            cur.execute("SELECT COUNT(id), AVG(price), MIN(price), MAX(price) FROM public.prices_validation")
        else:
            return {"error": "Invalid data_type. Choose between 'csv' or 'validation'."}

        overall_statistics = cur.fetchone()
        conn.close()
        return {"count": overall_statistics[0], "avg": overall_statistics[1], "min": overall_statistics[2],
                "max": overall_statistics[3]}
    except Exception as e:
        logging.error("Error: %s", e)
        return "Error getting overall statistics from DB!"

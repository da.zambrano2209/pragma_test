CREATE TABLE public.prices (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP,
    user_id INT,
    price FLOAT,
    source_file TEXT
);

CREATE TABLE public.statistics (
    row_count INT,
    avg_price FLOAT,
    min_price FLOAT,
    max_price FLOAT
);

CREATE TABLE public.prices_validation (
    id UUID PRIMARY KEY,
    created_at TIMESTAMP,
    user_id INT,
    price FLOAT,
    source_file TEXT
);

CREATE TABLE public.statistics_validation (
    row_count INT,
    avg_price FLOAT,
    min_price FLOAT,
    max_price FLOAT
);